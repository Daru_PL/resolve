﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHealth : MonoBehaviour
{

    public float maxHealth = 500;
    float currenthealth;
    public float moveSpeed = 5f;

    public GameObject deathEffect;

    public Transform player;


    private Rigidbody2D rb;
    private Vector2 movement;

    public bool isInvulnerable = false;



    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        currenthealth = maxHealth;
    }

    public void TakeDamage(float damage)
    {
        currenthealth -= damage;

        if (isInvulnerable)
            return;

        currenthealth -= damage;

        if (currenthealth <= 200)
        {
            GetComponent<Animator>().SetBool("IsEnraged", true);
            moveSpeed = 10f;

        }

        if (currenthealth <= 0)
        {
            Die();
        }



    }


    void Update()
    {

        Vector3 direction = player.position - transform.position;

        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        rb.rotation = angle;

        direction.Normalize();
        movement = direction;

    }

    private void FixedUpdate()
    {
        moveCharacter(movement);
    }

    void moveCharacter(Vector2 direction)
    {
        rb.MovePosition((Vector2)transform.position + (direction * moveSpeed * Time.deltaTime));
    }

    void Die()
        {
            Instantiate(deathEffect, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

    

}