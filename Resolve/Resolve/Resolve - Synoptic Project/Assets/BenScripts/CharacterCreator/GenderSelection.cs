﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenderSelection : MonoBehaviour {

	public Text genderChosen;

	public static bool maleChosen = false;
	public static bool femaleChosen = false;

	public void Male()
	{
		maleChosen = true;
		femaleChosen = false;
		genderChosen.text = "You have chosen: Male";
		Debug.Log(maleChosen.ToString() + femaleChosen);
	}

	public void Female()
	{
		femaleChosen = true;
		maleChosen = false;
		genderChosen.text = "You have chosen: Female";
		Debug.Log(maleChosen.ToString() + femaleChosen);
	}
}
