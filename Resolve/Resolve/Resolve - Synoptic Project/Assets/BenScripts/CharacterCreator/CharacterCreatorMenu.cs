﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class CharacterCreatorMenu : MonoBehaviour {

	public GameObject character;
	public List<OutfitChanger> outfitChangers = new List<OutfitChanger>();

	public void Submit()
	{
		SceneManager.LoadScene("CCTest");
	}

}
