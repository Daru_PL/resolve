﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum BodyArea
{
    Hood,
    Torso,
    Arms,
    Boots,
    Deafult
}
public class OutfitChanger : MonoBehaviour
{
    [SerializeField] private BodyArea bodyArea;
	[Header("Sprite to Change")]
	public SpriteRenderer bodyPart;

	[Header("Sprites to Cycle Through")]
	public List<Sprite> options = new List<Sprite>();

	private int currentOption = 0;

    private void Start()
    {
        CharacterCarry.UpdateCharacter(bodyArea, options[0]);
    }

    public void NextOption()
	{
		currentOption++;
		if(currentOption >= options.Count)
		{
			currentOption = 0;
		}

		bodyPart.sprite = options[currentOption];
        //Transfers currect character outfit
        CharacterCarry.UpdateCharacter(bodyArea, bodyPart.sprite);
    }

	public void PreviousOptions()
	{
		currentOption--;
		if(currentOption <= 0)
		{
			currentOption = options.Count - 1;
		}

		bodyPart.sprite = options[currentOption];
        //Transfers currect character outfit
        CharacterCarry.UpdateCharacter(bodyArea, bodyPart.sprite);
    }
}
