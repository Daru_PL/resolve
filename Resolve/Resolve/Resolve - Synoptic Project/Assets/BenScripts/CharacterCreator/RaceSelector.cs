﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaceSelector : MonoBehaviour {

	public Text raceChosen;

	public static bool human = false;
	public static bool dragonborn = false;
	public static bool elf = false;

	public void Human()
	{
		human = true;
		dragonborn = false;
		elf = false;
		raceChosen.text = "You picked: Human";
	}

	public void Dragonborn()
	{
		dragonborn = true;
		human = false;
		elf = false;
		raceChosen.text = "You picked: Dragonborn";
	}

	public void Elf()
	{
		elf = true;
		dragonborn = false;
		human = false;
		raceChosen.text = "You picked: Elf";
	}
}
