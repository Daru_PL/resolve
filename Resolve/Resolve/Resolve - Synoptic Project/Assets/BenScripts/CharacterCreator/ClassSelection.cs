﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClassSelection : MonoBehaviour {

	public Text classChosen;

	public static bool archer = false;
	public static bool knight = false;
	public static bool mage = false;

	public void Archer()
	{
		archer = true;
		knight = false;
		mage = false;
		classChosen.text = "You picked: Archer";
	}

	public void Knight()
	{
		knight = true;
		archer = false;
		mage = false;
		classChosen.text = "You picked: Knight";
	}

	public void Mage()
	{
		mage = true;
		archer = false;
		knight = false;
		classChosen.text = "You picked: Mage";
	}
}
