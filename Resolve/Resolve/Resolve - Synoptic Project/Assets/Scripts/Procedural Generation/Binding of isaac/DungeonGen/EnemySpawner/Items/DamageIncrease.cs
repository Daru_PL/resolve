﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageIncrease : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            Player.damage += 10f;
            Debug.Log("Damage has been increased by: " + Player.damage);
            Destroy(gameObject);
        }
    }

}
