﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour
{
	GameObject _playerObj;
	Player _playerScript;

	void Start()
	{
		_playerObj = GameObject.FindGameObjectWithTag("Player");
		_playerScript = _playerObj.GetComponent<Player>();
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Player")
		{
			_playerScript.SavePlayer();
			SceneManager.LoadScene(5);
		}
	}
}
