﻿using UnityEngine;

[CreateAssetMenu(fileName = "DungeonGenerationData.asset", menuName = "DungeoneGenerationData/Dungeon Data")]

public class DungeonGenerationData : ScriptableObject
{
    public int numberOfCrawlers;

    public int iterationMin;

    public int iterationMax;
}
