﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

	public enum DoorType
	{
		left, right, top, bottom
	}

	public DoorType doorType;

	public Collider2D doorCollider;

	private GameObject player;

    [SerializeField] GameObject doorCover;

	private float widthOffset = 4f;

    [SerializeField] private bool isClosed;

	private void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		doorCover.SetActive(isClosed);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Player")
		{
			switch (doorType)
			{
				case DoorType.bottom:
					player.transform.position = new Vector2(player.transform.position.x, player.transform.position.y - widthOffset);
					break;
				case DoorType.left:
					player.transform.position = new Vector2(player.transform.position.x - widthOffset, player.transform.position.y);
                    break;
				case DoorType.right:
					player.transform.position = new Vector2(player.transform.position.x + widthOffset, player.transform.position.y);
                    break;
				case DoorType.top:
					player.transform.position = new Vector2(player.transform.position.x, player.transform.position.y + widthOffset);
					break;
			}
            Debug.Log(player.transform.position);
        }
	}

    public bool Closed
    {
        get { return isClosed; }
        set
        {
            isClosed = value;
            doorCover.SetActive(value);
        }
    }
}
