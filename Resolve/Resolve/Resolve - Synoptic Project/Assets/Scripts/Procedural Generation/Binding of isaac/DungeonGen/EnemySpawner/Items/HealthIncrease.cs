﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthIncrease : MonoBehaviour {

    HealthBar _healthBar;
    GameObject _healthBarObj;

	Player _player;
	GameObject _playerObj;

    void Start()
    {
        _healthBarObj = GameObject.Find("Healthbar");
        _healthBar = _healthBarObj.GetComponent<HealthBar>();

		_playerObj = GameObject.FindGameObjectWithTag("Player");
		_player = _playerObj.GetComponent<Player>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            _player.maxHealth += 15;
            _healthBar.SetMaxHealth(_player.maxHealth);
            _healthBar.SetHealth(_player.currentHealth);
            Debug.Log("You gained " + _player.maxHealth + " max HP.");
            Debug.Log("Current HP: " + _player.currentHealth);
            Destroy(gameObject);
        }
    }
}
