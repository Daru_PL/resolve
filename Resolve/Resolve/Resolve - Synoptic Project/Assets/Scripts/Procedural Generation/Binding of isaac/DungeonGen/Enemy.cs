﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float maxHealth = 100;
    float currenthealth;

    public Transform player;
    public float moveSpeed;

    private Rigidbody2D rb;
    private Vector2 movement;

    public bool notInRoom = true;

    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        currenthealth = maxHealth;
    }

    public void TakeDamage(float damage)
    {
        currenthealth -= damage;
    }

    void Die()
    {
        Debug.Log("Enemy Died!");
        Player.level.AddExp(40);
        Destroy(gameObject);
    }

    
    void Update()
    {
        GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
        player = playerObject.transform;
        if (!notInRoom)
        {
            moveSpeed = 3f;
            Vector3 direction = player.position - transform.position;

            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            //rb.rotation = angle;

            direction.Normalize();
            movement = direction;

            if (currenthealth <= 0)
            {
                Die();
            }
        }
        else
        {
            moveSpeed = 0f;
        }
    }

    private void FixedUpdate()
    {
        moveCharacter(movement);
    }

    void moveCharacter(Vector2 direction)
    {
        rb.MovePosition((Vector2)transform.position + (direction * moveSpeed * Time.deltaTime));
    }
}
