﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoshUI : MonoBehaviour {

    public Text gold;

    int _goldAmount;

	int _coinsAmount;

	void Start()
	{
		_coinsAmount = GetComponent<Dosh>().money;
	}

    void Update()
    {
        _goldAmount = _coinsAmount;
		//Debug.Log(goldAmount);
        gold.text = "Gold: " + _goldAmount.ToString();
    }
}
