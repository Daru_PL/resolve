﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
	public float health;
	public float damage;
	public int gold;
	public int playerLevel;

	public PlayerData (Player player)
	{
		health = player.currentHealth;
		damage = player.AttackDamage;
		playerLevel = player.currLevel;
	}
}
