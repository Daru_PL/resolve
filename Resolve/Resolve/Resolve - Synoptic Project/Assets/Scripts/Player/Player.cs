﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Player : Character
{
    public Animator animator;
    public static Level level;

    public float maxHealth;
    public float currentHealth;
	public int currLevel;
	public static float damage;

    public Enemy enemy;

    public Transform attackPoint;
    public float attackRange = 0.5f;
    public float AttackDamage = 40f;

    public LayerMask enemyLayers;

    public HealthBar healthbar;

	Vector3 _orgPlayerScale;
	Vector3 _tempScale;

	bool _runOnce = false;

	//Ben Scripts

	public float moveSpeed = 250f;
	public Rigidbody2D rb;
	Vector2 movement;
	public float attackrate = 2f;
	float nextAttacktime = 0f;

	// Use this for initialization
	void Awake ()
    {
		if (_runOnce == false)
		{
			_orgPlayerScale = gameObject.transform.localScale;
			_tempScale = gameObject.transform.localScale;
			_tempScale.x *= -1;

			maxHealth = 100f;

			level = new Level(1, OnLevelUp);

			_runOnce = true;
		}
	}

    void Start()
    {
		LoadPlayer();

		currentHealth = maxHealth;
		healthbar.SetMaxHealth(maxHealth);

		Debug.Log("Damage: " + AttackDamage);
		Debug.Log("Health: " + currentHealth);
	}

    public void OnLevelUp()
    {
        print("I Leveled Up!");

		AttackDamage += 5f;
		maxHealth += 10f;
		attackrate += 0.5f;
		moveSpeed += 5f;

		currentHealth = maxHealth;
		healthbar.SetHealth(currentHealth);
	}

    void Update()
    {
        if ((currentHealth <= 0))
        {
            level.AddExp(-100);
        }

		if (Time.time >= nextAttacktime)
		{
			if (Input.GetButtonDown("Fire1"))
			{
				Attack();
				nextAttacktime = Time.time + 1f / attackrate;
				Debug.Log("weapon used");
			}
		}

		if (Input.GetKeyDown("a"))
		{
			gameObject.transform.localScale = _tempScale;
		}
		
		if (Input.GetKeyDown("d"))
		{
			gameObject.transform.localScale = _orgPlayerScale;
		}

		movement.x = Input.GetAxisRaw("Horizontal");
		movement.y = Input.GetAxisRaw("Vertical");

		currLevel = level.currentLevel;

		//animator.SetFloat("Horizontal", movement.x);
		//animator.SetFloat("Vertical", movement.y);
		//animator.SetFloat("Speed", movement.sqrMagnitude);
	}
	void FixedUpdate()
	{
		rb.velocity = (movement * Time.fixedDeltaTime) * moveSpeed;
	}

	void Attack()
    {
        //animator.SetTrigger("Attack");

        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);

        foreach (Collider2D enemy in hitEnemies)
        {
            enemy.GetComponent<Enemy>().TakeDamage(AttackDamage);
        }
    }

    private void OnCollisionEnter2D(Collision2D obj)
    {
        if (obj.gameObject.tag == "Enemy")
        {
            TakeDamage(20);
        }
		if (obj.gameObject.tag == "Boss")
		{
			TakeDamage(40);
		}
	}

    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;

        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }

    void TakeDamage(int damage)
    {
        currentHealth -= damage;

        healthbar.SetHealth(currentHealth);

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        Debug.Log("Player Died!");
        Destroy(gameObject);
    }

	public void SavePlayer()
	{
		SaveSystem.SavePlayer(this);
	}

	public void LoadPlayer()
	{
		PlayerData data = SaveSystem.LoadPlayer();

		currentHealth = data.health;
		AttackDamage = data.damage;
	}
}