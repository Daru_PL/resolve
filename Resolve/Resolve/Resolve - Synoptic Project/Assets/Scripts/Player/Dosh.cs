﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dosh : MonoBehaviour {

    public int money;

	// Use this for initialization
	void Start ()
    {
        money = 0;
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            money++;
            Destroy(gameObject);
            Debug.Log("You have collected " + money);
        }
    }
}
