﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCarry : MonoBehaviour
{
    [SerializeField] SpriteRenderer hoodObj, torsoObj, armObj, bootObj;
    [SerializeField] static Sprite hoodSprite, torsoSprite, armSprite, bootSprite;

    private void Start()
    {
        if (hoodSprite == null) return;

        hoodObj.sprite   = hoodSprite;
        torsoObj.sprite  = torsoSprite;
        armObj.sprite    = armSprite;
        bootObj.sprite   = bootSprite;
    }

    public static void UpdateCharacter(BodyArea a_bodyPart, Sprite a_sprite)
    {
        switch (a_bodyPart)
        {
            case BodyArea.Hood: hoodSprite = a_sprite;
                break;
            case BodyArea.Torso: torsoSprite = a_sprite;
                break;
            case BodyArea.Arms: armSprite = a_sprite;
                break;
            case BodyArea.Boots: bootSprite = a_sprite;
                break;
            case BodyArea.Deafult: Debug.Log("Error in CharacterCarry");
                break;
        }
    }
}
